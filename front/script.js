/**
 *
 * @source: script.js
 *
 * @licstart  The following is the entire license notice for the 
 *  JavaScript code in this page.
 *
 * The JavaScript code in this page is free software: you can
 * redistribute it and/or modify it under the terms of the GNU
 * General Public License (GNU GPL) as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.  The code is distributed WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.
 *
 * As additional permission under GNU GPL version 3 section 7, you
 * may distribute non-source (e.g., minimized or compacted) forms of
 * that code without the copy of the GNU GPL normally required by
 * section 4, provided you include this license notice and a URL
 * through which recipients can access the Corresponding Source.
 *
 * @licend  The above is the entire license notice
 * for the JavaScript code in this page.
 *
 */

var connection = null;
var retry = 0;
var unread = 0;
var title = document.title;
var last_send = null;
var chat_usr;
var chat_room;

function enableChat(enable)
{
    document.getElementById("login-body")
        .style.display = enable ? "none" : "block";
    document.getElementById("chat-body")
        .style.display = enable ? "block" : "none";

    if (enable)
    {
        chat_usr = document.getElementById("person_id").value
        chat_room = document.getElementById("room_id").value
        
        document.getElementById("username").innerHTML =
            "<i>User: </i>"
            + chat_usr;

        document.getElementById("room").innerHTML =
            "<i>Room: </i>"
            + chat_room;
    }
}

function connect()
{
    connection = new WebSocket('ws://' + WS_SERVER + ':' + WS_PORT);
    
    connection.onopen = function ()
    {
        console.debug('======== success ========', connection);
        enableChat(true);
        document.getElementById("input").focus();        
        retry = 1;

        if(last_send != null)
        {
            connection.send(last_send);
        }
    };

    connection.onmessage = function (e)
    {   
        let text_area = document.getElementById("textarea");

        if(e.data == last_send)
        {
            last_send = null;
        }

        let msg_obj = JSON.parse(e.data);

        if(msg_obj.room == chat_room)
        {
            
            let msg_txt = '[' + msg_obj.usr + '] ' + msg_obj.msg;
            text_area.value += '\n' + msg_txt + '\n';

            text_area.scrollTop = text_area.scrollHeight;

            unread++;
        }
    };

    connection.onerror = function (e)
    {        
        console.debug('====== error =========', connection, e);
    };

    connection.onclose = function(e) 
    {
        console.debug('====== close =========', connection, e);
        if(--retry <= 0)
        {
            enableChat(false);
            document.getElementById("button-connect").focus();
        }
        else
        {
            connect();
        }
    };

    return false;
}

function send2server(txt)
{   
    last_send = txt;
    connection.send(txt);
}

function send()
{   
    let msg = document.getElementById("input").value;
    
    if(msg != "")
    {
        let msg_obj = {usr: chat_usr,
                       msg: msg,
                       room: chat_room};

        send2server(JSON.stringify(msg_obj));

        document.getElementById("input").value = "";
    }
    
    return false;
}


function notifyUnread()
{
    if (document.hidden && (unread > 0))
    {
        var newTitle = '(' + unread + ') ' + title;
        document.title = newTitle;
    }
    else
    {
        unread = 0;
        document.title = title;
    }
}

function updateTitle()
{
    update = setInterval(notifyUnread, 2000);
}
updateTitle()
