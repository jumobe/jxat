// -*- mode:c++;c-file-style:"linux";indent-tabs-mode:nil;c-basic-offset:2 -*-

#include "jwsexception.h"

#include <jlpp.h>
#include <sstream>

jwsexception::jwsexception(const std::string msg)
{  
  this->msg = msg;
  jlpp::err << this->msg << "\n";
}

const char* jwsexception::what() const noexcept
{
  return this->msg.c_str();
}

void jwsexception::socket_error(const int code, const std::string stg)
{  
  if (code < 0)
  {
    std::stringstream ss;
    ss << "Erro ao abrir socket (" << stg  << "): " << code;
    throw jwsexception(ss.str());
  }
}
