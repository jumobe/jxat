
#include "jwsencode.h"

#include <openssl/sha.h>
#include <iostream>
#include <jlpp.h>
#include <sstream>
#include <locale>
#include <cstring>
#include <cctype>



std::string jwsencode::base64_encode(const unsigned char *data,
                                     size_t input_length) {
  const std::string enc_tb =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
  
  const int mod_table[] = {0, 2, 1};

  const size_t output_length = 4 * ((input_length + 2) / 3);
 
  char encoded_data[output_length];
 
  for (unsigned int i = 0, j = 0; i < input_length;)
  {
 
    uint32_t octet_a = i < input_length ? (unsigned char)data[i++] : 0;
    uint32_t octet_b = i < input_length ? (unsigned char)data[i++] : 0;
    uint32_t octet_c = i < input_length ? (unsigned char)data[i++] : 0;
 
    uint32_t triple = (octet_a << 0x10) + (octet_b << 0x08) + octet_c;
 
    encoded_data[j++] = enc_tb[(triple >> 3 * 6) & 0x3F];
    encoded_data[j++] = enc_tb[(triple >> 2 * 6) & 0x3F];
    encoded_data[j++] = enc_tb[(triple >> 1 * 6) & 0x3F];
    encoded_data[j++] = enc_tb[(triple >> 0 * 6) & 0x3F];
  }  
  
  for (int i = 0; i < mod_table[input_length % 3]; i++)
  {
    encoded_data[output_length - 1 - i] = '=';
  }

  std::string resp(encoded_data);

  auto it = resp.end();

  while(*it-- != '=' );
  
  resp.erase(it+2, resp.end());
  
  return resp;
}


const std::string jwsencode::calc_handshake_hash(const std::string key)
{
  std::string hash = key + "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";
  
  unsigned char sha1_resp[SHA_DIGEST_LENGTH]; 

  SHA_CTX ctx;
  SHA1_Init(&ctx);
  SHA1_Update(&ctx, hash.c_str(), hash.length());
  SHA1_Final(sha1_resp, &ctx);
  
  return base64_encode(sha1_resp, SHA_DIGEST_LENGTH);
}

std::string jwsencode::parseMsg(std::string& msg)
{
  unsigned short i, j;

  std::locale loc("");

  const unsigned short first = msg[0] & 0xff;
    
  const unsigned short second = msg[1] & 0xff;

  const unsigned short size_msg = second & 0X7f;
  
  std::string content;
    
  for(i = 0, j = 6; i < size_msg; i++, j++)
  {
    const char c = msg[j] ^ msg[(i & 0x3) + 2];
    content.push_back(c);
  }
  
  jlpp::dbg << "byte[0]  = [" << std::to_string(first)    << "]";
  jlpp::dbg << "byte[1]  = [" << std::to_string(second)   << "]";
  jlpp::dbg << "size msg = [" << std::to_string(size_msg) << "]";

  bool is_print = true;

  std::stringstream ss;
  ss << "msg(bytes) = [";
  for(auto it: content)
  {
    const char v1 = it & 0xff;
    is_print = is_print && (std::isprint(v1, loc) || std::isblank(v1));
    unsigned short v2 = it & 0xff;
    ss << std::to_string(v2)
       << "(" << std::to_string(it) << "), ";
  }
  jlpp::dbg << ss.str() << "]\n";

  jlpp::dbg << "msg = [" << content << "]\n";
  
  if (!is_print)
  {
    return "";
  }

  msg.erase(msg.begin() + j, msg.end());

  return content;
}

std::string jwsencode::encodeMsg(std::string msg)
{
  std::string response;
  response.push_back(129);
  response.push_back(msg.size() & 0x7f);
  for(auto c: msg)
  {
    response.push_back(c);
  }
  return response;
}

