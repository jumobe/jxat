// -*- mode:c++;c-file-style:"linux";indent-tabs-mode:nil;c-basic-offset:2 -*-

#include "jwsocket.h"

#include "jwsactor.h"
#include "jwsencode.h"
#include "jwsexception.h"

#include <jlpp.h>
#include <chrono>
#include <iostream>
#include <thread>

#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>


jwsocket::jwsocket(const unsigned int port) noexcept
{
  this->port = port;
  this->jwslisten();
}

jwsocket::~jwsocket()
{
  close(this->sockfd);
}

void jwsocket::jwslisten()
{
  try
  {
    this->sockfd = socket(AF_INET, SOCK_STREAM, 0);

    jwsactor actor;
    
    this->allfds[this->sockfd] = actor;

    jwsexception::socket_error(this->sockfd, "socket");

    this->serv_addr.sin_family = AF_INET;
    this->serv_addr.sin_addr.s_addr = INADDR_ANY;
    this->serv_addr.sin_port = htons(this->port);

    jwsexception::socket_error(
      bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)), "bind");

    jwsexception::socket_error(
      listen(this->sockfd, 50), "listen");   

    this->readfds();

    // Thread para verificar fd sem eventos por muito tempo
    std::thread kill_lazy_fd([=]{ this->verify_lazy(); });
    
    while(1)
    {
      try
      {
        jlpp::dbg << "Esperando evento fd... \n";

        int retval = select(this->n_rfds + 1, &this->rfds, NULL, NULL, NULL);

        jlpp::dbg << "Evento em [" 
                  << std::to_string(retval)
                  << "] fds\n";
        
        jwsexception::socket_error(retval, "select");

        this->check_fds(retval);
        
        this->readfds();
      }
      catch(...)
      {
        kill_lazy_fd.join();
        this->close_fds();
        this->check_fds(1);
      }
    }

  }
  catch (const std::exception& e)
  {
    jlpp::err << e.what() << "\n";
    this->close_fds();
  }
}

void jwsocket::check_fds(const int nfdevents)
{

  const auto last_fd = this->allfds.end();

  int fdevents = 0;
  
  for(auto fds = this->allfds.begin();
      (fds != last_fd) && (fdevents < nfdevents); fds++)
  {
    jlpp::dbg << "Laco no fd: ["
              << std::to_string(fds->first)
              << "] \n";
      
    if ( FD_ISSET(fds->first, &this->rfds) )
    {

      jlpp::dbg << "Socket ["
                << std::to_string(fds->first)
                << "] esta setado \n";
      
      if (this->is_main_fd(fds->first))
      {

        jlpp::dbg << "Nova conexao: \n";
        
        int newsockfd = accept(this->sockfd, NULL, NULL);    
      
        jwsexception::socket_error(newsockfd, "accept");
            
        jwsactor actor(newsockfd);

        this->allfds[newsockfd] = actor;
      }
      else
      {
        std::string msg = fds->second.read();        
        
        const unsigned int fb =  msg[0] & 0xff;
        
        if(fb != 129)
        {
          jlpp::dbg << "Close connection: ["
                    << std::to_string(fds->first)
                    << "] \n";
          this->close_fd(fds->first);
        }
        else
        {
          this->broadcast(msg);
        }
      }
      
      fdevents++;
    }
  }  
}

void jwsocket::readfds()
{
  FD_ZERO(&this->rfds);
  this->n_rfds = 0;
  
  for(auto it: this->allfds)
  {
    FD_SET(it.first, &this->rfds);
    this->n_rfds = std::max(this->n_rfds, it.first);
  }
}


void jwsocket::broadcast(std::string msg) const
{
  const std::string content_msg = jwsencode::parseMsg(msg);
  
  if(content_msg.size() > 0)
  {  
    for(auto fd : this->allfds)
    {
      if(!this->is_main_fd(fd.first))
      {        
        fd.second.send_msg(jwsencode::encodeMsg(content_msg));
      }
    }
  }
}

void jwsocket::close_fds()
{
  for(auto fd = this->allfds.begin(); fd != this->allfds.end(); fd++)
  {
    if(!this->is_main_fd(fd->first))
    {
      close(fd->first);
    }
  }
}

/**
 * Metodo continuo para eliminacao de fd ocioso.
 */
void jwsocket::verify_lazy()
{
  using namespace std::chrono_literals;
  while(true)
  {
    for(auto fd = this->allfds.begin(); fd != this->allfds.end(); fd++)
    {    
      if(!this->is_main_fd(fd->first)
         && (fd->second.lazy_time() > this->max_sec_lazy_fd)) {
      
        jlpp::dbg << "kill lazy["
                  << std::to_string(fd->first)
                  << "] \n";
        this->close_fd(fd->first);
      }
    }
    std::this_thread::sleep_for(1min);
  }
}


void jwsocket::close_fd(const unsigned int fd)
{
  close(fd);
  this->allfds.erase(fd);
}


bool jwsocket::is_main_fd(const unsigned int fd) const
{
  const unsigned int mfd = static_cast<unsigned int>(this->sockfd);

  return (fd == mfd);
}
